// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  isMockEnabled: true, // You have to switch this, when your real back-end is done
  authTokenKey: 'authce9d77b308c149d5992a80073637e4d5',
  HotelUrl: 'https://trivo.org/Webservices/Hotel/HotelHandler.asmx/',
  TourUrl: 'https://trivo.org/Webservices/Activity/Handler/GetActivityHandler.asmx/',
  locationUrl: 'https://trivo.org/Webservices/genral/locationHandler.asmx/',
  QoutationUrl: 'https://trivo.org/Webservices/Qoutation/qoutationhandler.asmx/',
  GenralUrl: 'https://trivo.org/Webservices/genral/ExchnageHandler.asmx/',
  TransferUrl: 'https://trivo.org/Webservices/Vehicle/VehicleHandler.asmx/',
  RestaurantsUrl: 'https://trivo.org/Webservices/Resturants/resturantsHandler.asmx/',
  AuthanticationUrl:'https://trivo.org/Webservices/Authorize/service/loginHandler.asmx/',
  AdminID: 232,
  UserName: "holidays@clickurtrip.com",
  Password: "India2007",
  uid: "operations@clickurtrip.com",
  password: "anamhiba2007"
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
