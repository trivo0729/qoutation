import {Injectable  } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Promise } from 'q';
import { Observable } from 'rxjs';
import { Search_Params } from '../models/lead/sightseeing';
import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class RestaurantService {
  Parent:number;
  Url:string;
  arrUserDetails: any;
  arrDetails: any ;
  Search_Params:Search_Params = new Search_Params();
  constructor(
    private httpClient: HttpClient,
    ){
      this.Url= environment.RestaurantsUrl;
      this.Parent= environment.AdminID;
      this.arrUserDetails = new Array();
      this.arrUserDetails.userName = environment.UserName;
      this.arrUserDetails.password = environment.Password;
      this.arrUserDetails.parentID = environment.AdminID;
    }
  /*Get All Restaurants*/ 
  getRestaurants(location:string,sCurrency:string): Observable<any>{
     debugger
     var arrSearch = {
        arrUserDetails:{
            userName: environment.UserName,
            password:environment.Password,
            parentID:environment.AdminID,
        },
        City: location ,
        sCurrency
     }
      return this.httpClient.post<any>(this.Url +"_Search",
      {
        arrSearch : arrSearch
      },
      {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
       })
      });
  }
  


}