import {Injectable  } from '@angular/core';
import { Tours, TourType, DateRange ,Slots, TicketType, TourTypes, Dates} from '../models/lead/sightseeing';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Promise } from 'q';
import { Observable } from 'rxjs';
import { Search_Params } from '../models/lead/sightseeing';
import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class ToursService {
  arrTours :Tours;
  arrTourType:TourType;
  arrDates: DateRange;
  Parent:number;
  Url:string;
  arrUserDetails: any;
  arrDetails: any ;
  Search_Params:Search_Params = new Search_Params();
  constructor(
    private httpClient: HttpClient,
    ){
      this.Url= environment.TourUrl;
      this.Parent= environment.AdminID;
      this.arrUserDetails = new Array();
      this.arrUserDetails.userName = environment.UserName;
      this.arrUserDetails.password = environment.Password;
      this.arrUserDetails.parentID = environment.AdminID;
    }
/*Get  Tour Images*/ 
GetTourName(Name:string): Observable<any>{
  return this.httpClient.post<any>( this.Url +"GetTours",
  {
    Name:Name,
    ParentID :environment.AdminID
  },{ headers: new HttpHeaders({'Content-Type': 'application/json'})});
}

// /*Get All Tours*/ 
// getLocation(Name:string): Observable<arrLocation[]>{
//   return this.httpClient.post<arrLocation[]>( this.Url +"GetLocations",
//   {
//       ParentID: this.Parent, Name
//   },{ headers: new HttpHeaders({'Content-Type': 'application/json'})});
// }

/*Get Popular  Tours*/ 
getPopularTours(): Observable<any>{
  return this.httpClient.post<any>(this.Url + "GetPopulars",
  {AdminID: this.Parent,},{ headers: new HttpHeaders({'Content-Type': 'application/json'})});
}

/*Save Popular Tours*/ 
SavePopularTour(ActivitID:number,MinPrice:number): Observable<any>{
  return this.httpClient.post<any>( this.Url +"SetPopularTour",
  {
    AdminID: this.Parent,
    ActivitID:ActivitID,
    MinPrice:MinPrice,
  },{ headers: new HttpHeaders({'Content-Type': 'application/json'})});
}

/*Get  Tour Images*/ 
GetImage(ActivitID:number): Observable<any>{
  return this.httpClient.post<any>( this.Url +"GetImages",
  {
    ActivityId:ActivitID,
  },{ headers: new HttpHeaders({'Content-Type': 'application/json'})});
}

  /*Get All Tours*/ 
  getTours(location:string,date:string,tourType:string,sCurrency:string): Observable<any>{
     debugger
     var arrSearch = {
        arrUserDetails:{
            userName: environment.UserName,
            password:environment.Password,
            parentID:environment.AdminID,
        },
        locationID: location ,
        date:date,
        tourType:tourType,
        sCurrency

     }
      return this.httpClient.post<any>(this.Url +"SearchTours",
      {
        arrSearch : arrSearch
      },
      {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
       })
      });
  }
  
/*Get  Tour Type By Tour*/ 
 getTourtype(ID: Number,ActivityDate:string): Observable<TourType[]>{
    return this.httpClient.post<TourType[]>(this.Url +"GetRateType",{ID,ActivityDate},{
        headers: new HttpHeaders({'Content-Type': 'application/json'})
    });
  } 
/*Get  All Tour Type*/ 
getAllTourtype(Location:string): Observable<TourTypes[]>{
  return this.httpClient.post<TourTypes[]>(this.Url +"GetTourType",{
    ParentID:this.Parent,
    LocationID:Location
  },{
      headers: new HttpHeaders({'Content-Type': 'application/json'})
  });
}


  /*Avail Dates*/ 
  getDateRange({ Ratetype, ID, Date,next }: { Ratetype: string; ID: Number; Date: string; next:boolean;}): Observable<Dates>{
    return this.httpClient.post<Dates>( this.Url +"DateArray",
    {ID,Date,Ratetype,next},{headers: new HttpHeaders({'Content-Type': 'application/json'})})
  }

  /*Avail Slots*/
  getSlots({ Type, ID, Date }: { Type: string; ID: Number; Date: string; }): Observable<Slots[]>{
    return this.httpClient.post<Slots[]>( this.Url +"SlotByDate",
    {
      ID,Date,Type 
    },
    {headers: new HttpHeaders({'Content-Type': 'application/json'})})
  }

  /*Slot  Rates*/
  getSlotRate({ Type, ID,SlotID, Date }: { Type: string; ID: Number;SlotID:Number; Date: string; }): Observable<TicketType[]>{
    return this.httpClient.post<TicketType[]>(this.Url +"GetRates",
    {
      arrUserDetails:{
        userName: environment.UserName,
        password:environment.Password,
        parentID:environment.AdminID,
    },
      Type,ID,SlotID,Date
    },{headers: new HttpHeaders({'Content-Type': 'application/json'})})
  } 

/*Slot  Rates*/
BookTours(arrBooking:any,CartID:String,arrParams:any[]): Observable<any>{
  return this.httpClient.post<any>(this.Url  +"Book",
  {
    arrBooking:arrBooking,
    CartID,
    arrParams
  },{headers: new HttpHeaders({'Content-Type': 'application/json'})})
}   



/*Get  Transfer Location*/ 
getTransferLocation(Type:string,TripType:string, LocationName:string,PickupID:string[]): Observable<any>{
  return this.httpClient.post<any>( environment.TransferUrl +"_getDestination",
  {
    Type,
    TripType,
    LocationName,
    PickupID
  },{ headers: new HttpHeaders({'Content-Type': 'application/json'})});
}

/*Get  Transfer Location*/ 
getTransferSearch(PickupID:number,DropID:number,TravelType:string ,PickupType:string,sCurrency:string): Observable<any>{
 var arrSearch= {
    TravelDate:'',
    PickupID,
    DropID,
    TravelType,
    PickupType,
    sCurrency,
    arrUserDetails:{
      UserName: environment.UserName,
      Password:environment.Password,
      ParentID:environment.AdminID,
  }
 }
  return this.httpClient.post<any>( environment.TransferUrl +"_getRates",
  {
    arrSearch : arrSearch
  },{ headers: new HttpHeaders({'Content-Type': 'application/json'})});
}

}