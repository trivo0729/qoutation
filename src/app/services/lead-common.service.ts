import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { HotelSearch } from '../models/lead/hotel.model';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LeadCommonService {

  constructor(private httpClient: HttpClient) { }

  Getdays(start: any, end: any) {
    var startdate = moment(start).toDate();
    var enddate = moment(end).toDate();
    const days = (enddate.getTime() - startdate.getTime()) / (1000 * 60 * 60 * 24);
    return parseInt(days.toString());
  }

  getHotelLocation(value: string): Observable<HotelSearch[]> {
    return this.httpClient.post<HotelSearch[]>(environment.HotelUrl + "GetDestination",
      {
        sPlaceName: value,
      }, { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) });
  }

  /*Get  Country*/
  getCountry(): Observable<any> {
    return this.httpClient.post<any>(environment.locationUrl + "GetCountry",
      {},
      {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      });
  }

  get(name: string) {
    let d = JSON.parse(localStorage.getItem(name));
    return d;
  }

  getHotels(Search: any, Requestno: number) {
    return this.httpClient.post<any>(environment.QoutationUrl + "HotelSearch",
      {
        arrSearch: Search,
        Requestno: Requestno,
        ParentId: 232
      },
      {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      });
  }

  saveQoutation(object: any) {
    return this.httpClient.post( environment.QoutationUrl +'/_Save',
      { arrLead: object },
      { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) });
  }

  getAgents() {
    return this.httpClient.post(environment.QoutationUrl + "AgentList",
      {
        ParentId: environment.AdminID
      },
      { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) });
  }

  getStaff() {
    return this.httpClient.post(environment.QoutationUrl + "StaffList",
      {
        ParentId: environment.AdminID
      },
      { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) });
  }

  getHotelList(Name: string, CityCode: string) {
    return this.httpClient.post(environment.QoutationUrl + "HotelList",
      {
        Name: Name,
        CityCode: CityCode,
        ParentId: environment.AdminID
      },
      { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) });
  }
  getLeads() {
    var arrUserDetails ={
        userName : environment.uid,
        password :environment.password,
        parentID : environment.AdminID
    }
    return this.httpClient.post( environment.QoutationUrl+  '/_getQoutes',
        {
            arrUserDetails
        },
        { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) });
}
}
