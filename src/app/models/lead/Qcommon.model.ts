export class User {
    sid: number;
    name: string;
    uid: string
}

export class Staff {
    id: string;
    value: string
}

export class hotel {
    id: string;
    value: string
}

export class arrCurrency {
    BaseCurrency: string;
    Currency: string;
    Exchange: number;
    LastUpdateDt: string;
    MailFlag: boolean;
    Rate: number;
}