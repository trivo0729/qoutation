
export class summary {
    FileNo: string;
    QueryNo: string;
    location: locations[];
}

export class locations {
    Name: string;
    LocationCode: string
    Service: service[];
    price: 0.00;
}

export class service {
    // LocationCode: string;
    Hotel: Hotel[];
    Tour: any[];
    Transfer : any[];
    Restaurant : any[];
}
export class Tour {

}

export class Hotel {
    name: string;
    checkin: string;
    checkout: string;
    code: string;
    room: number;
    mealname: string;
    mealcode: string;
    Rooms: Rooms[];
    shortinfo: string;
    total: any;
}

export class Rooms {
    no: number;
    adult: number;
    child: number;
    infant: number;
    _child: string;
    price: any;
    RoomType: any;
    RateChildWithBed: any;
    RateChildWithoutBed: any;
    RoomRate: any;
    ExtraBedRate: any;
    Total: any;
    Currency: any;
}

export class child {
    no: number;
    age: number
}
