export class Hotel {
    name: string;
    code: string;
    room: number;
    mealname: string;
    mealcode: string;
    Rooms: Rooms[];
}

export class Rooms {
    no: number;
    adult: number;
    child: number;
    infant: number;
    _child: child[];
    RoomType: string;
    RateChildWithBed: any;
    RateChildWithoutBed: any;
    RoomRate: any;
    ExtraBedRate: any;
    Total: any;
    Currency: any;
    description: any;
    rateAvailable: boolean;
    response: boolean;
    loading: boolean;
}

export class child {
    no: number;
    age: number
}

export class HotelSearch {
    id: number;
    value: string;
}


export class objSearch {
    HotelCode?: any[];
    HotelName: string;
    Destination: Destination;
    Checkin: string;
    Checkout: string;
    nationality: string;
    Nights: number;
    Adults: number;
    Childs: number;
    Supplier: string;
    MealPlan: string;
    CurrencyCode: string;
    SearchType: string;
    username: string;
    Password: string;
    Rooms?: (Occupancy)[] | null;
    TockenID: string;
    UserIP: string;
}
export class Destination {
    Code: string;
    City: string;
    Country: string;
}
export class Occupancy {
    RoomCount: number;
    AdultCount: number;
    ChildCount: number;
    ChildAges?: (number)[] | null;
}


export class objTraveller {
    Location: string;
    Date: any;
    Nationality: string;
    Guests: any;
}

export class arrRooms {
    adults: number;
    childs: number[];
}

export class Traveller {
    adults: number;
    childs: Childs[];
}

export class Childs {
    count: number;
    childAge: number;
    ages: any;
}

