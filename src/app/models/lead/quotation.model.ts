export class arrLead {
    _id: string;
    FileNo: string;
    CustomerId: number;
    CustomerType: string;
    ParentId: any;
    CreateDate: string;
    CreateBy: string;
    GuestNationality: string;
    Source: string;
    PackageTitle: string;
    Query: string;
    FileStatus: string;
    Destination?: Destination[];
    PurposeVist: string;
    FollowUp?: FollowUp[];
    Queries?: Queries[];
    Email: string;
    Mobile: string;
    Currency: string;
}

// export class Source {
//     name: string;
//     Detail: string;
// }
export class Destination {
    CityId: string;
    CityName: string;
    Country: string;
    State: string;
    Start: string;
    End: string;
}
export class FollowUp {
    NextDate: string;
    LastDate: string;
    By: By;
}
export class By {
    User: string;
    On: string;
}
export class Queries {
    QueryNo: string;
    Handler: string;
    Date: string;
    StartDate: string;
    EndDate: string;
    PaxDetails: PaxDetails;
    Destination?: (Destination)[] | null;
    Status: string;
    Qoute: Qoute;
}
export class PaxDetails {
    LaedPaxName: string;
    Adult: any;
    Child: any;
    Infant: any;
}
export class Qoute {
    Discription: string;
    Sevices?: (string)[] | null;
}

export class locations {
    selectedItems = [];
    Date: any;
    option: any;
}

export class arrLeads{
    Qoutation : arrLead;
    Agent: string;
    Priority : number;
    CreateBy :string;
    PaxDetails :any;
    Destination: any;
}