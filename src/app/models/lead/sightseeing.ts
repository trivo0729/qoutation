export class Tours{
    ID:number;
    Name:string;
    arrTheme:any[];
    Supplier:string;
    Price:string;
    
    arrTicket:TourType[];
    arrSlot: Slots[];
    arrRates:TicketType[];

    show:boolean;

    arrSelRates:TicketType[];
    selectedTicket :TicketType;
    selSlot:number;
    selSlotText:string;
    selRateType:string;
    selTicket:string;
    bselected:boolean;
} 
export class TourType {
    Type: string;
}
export class Dates {
    sYear: string;
    arrDates: DateRange[];
}
export class DateRange {
    sDate: string;
    fDate: string;
    Month: string;
    Day: string;
    Date: string;
    Available: boolean
}
export class Slots {
    ID: number;
    RateType:string;
    SlotsStartTime: string;
    SlotsEndTime: string;
}
export class TicketType {
    RateType:string;
    Adult:number;
    Child1:number;
    Child2:number;
    Infant:number;
    Ticket: string;
    SlotID :number;
    TicketID: string;
    InventoryType: String;
    MinPax: Number;
    MaxPax: Number;
    available: Number;
    arrayPax: PaxTyp[];
    Currency: string;
    PlanId: number;
    nonRefundable: boolean;
    arrCancellationPolicy: Cancellation[];
    Total: number;
    Inclusions: string;
    Exclusions: string;
}
export class Cancellation {
    AmountToCharge: string;
    CancelationPolicy: string;
    ChargesType: string;
    DaysPrior: string;
    IsDaysPrior: string;
    PercentageToCharge: string;
    RefundType: string
}
export class PaxTyp {
    Pax_Type: string;
    Rate: string;
    Age: string;
    Count: number = 1;
    Total=0.00;
}
export class TourTypes {
    Icon: string;
    Name: string;
    ActivityCount: number;
    MinPrice: number;
    checked: Boolean = false;
}

export class objTour {
    Date: any;
    Location: any;
    LocationName: any;
    TourType: any;
}
export class Search_Params {
    LocationID: string;
    ActivityID:string;
    TourType: string;
    sDate: string;
    LocationName: string;
    Time: string
    Adults: number;
    Child: number;
    sCurrency:string;
    constructor() {
        this.LocationID = "";
        this.TourType = "";
        this.sDate = "";
        this.LocationName = "";
        this.Time = "12:00";
        this.Adults = 1;
        this.Child = 0;
    }

}