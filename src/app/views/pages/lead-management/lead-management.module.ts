import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PartialsModule } from '../../partials/partials.module';
import { RouterModule, Routes } from '@angular/router';
import { LeadCreateComponent } from './lead/lead-create/lead-create.component';
import { LeadManagementComponent } from './lead-management.component';
import {
  MatInputModule, MatMenuModule, MatIconModule, MatButtonModule, MatTooltipModule,
  MatAutocompleteModule, MatRadioModule, MatSelectModule, MatCheckboxModule
  , MatExpansionModule, MatListModule, MatProgressBarModule, MatChipsModule, MatBadgeModule, MatTableModule, MatPaginator, MatPaginatorModule
} from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { Daterangepicker } from 'ng2-daterangepicker';
import { HotelComponent } from './hotel/hotel.component';
import { SightseeingComponent } from './sightseeing/sightseeing.component';
import { TransfersComponent } from './transfers/transfers.component';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import {  NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { RestaurantsComponent } from './restaurants/restaurants.component';
import { LeadListComponent } from './lead-list/lead-list.component';


const routes: Routes = [
  {
    path: '',
    component: LeadManagementComponent,
    children: [
      {
        path: '',
        redirectTo: 'create-lead',
        pathMatch: 'full'
      },
      {
        path: 'create-lead',
        component: LeadCreateComponent
      },
      {
        path: 'hotel',
        component: HotelComponent
      },
      {
        path: 'sightseeing',
        component: SightseeingComponent
      },
      {
        path: 'transfers',
        component: TransfersComponent
      },
      {
        path: 'restaurants',
        component: RestaurantsComponent
      },
      {
        path: 'leads',
        component: LeadListComponent
      }
    ]
  }
];

@NgModule({

  imports: [
    CommonModule,
    PartialsModule,
    RouterModule.forChild(routes),
    MatInputModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    MatTooltipModule,
    MatCheckboxModule,
    MatSelectModule,
    MatAutocompleteModule,
    MatRadioModule,
    MatSelectModule,
    MatListModule,
    MatChipsModule,
    MatExpansionModule,
    MatProgressBarModule,
    MatTableModule,
    MatPaginatorModule,
    FormsModule,
    ReactiveFormsModule,
    NgxDropzoneModule,
    Daterangepicker,
    AngularMultiSelectModule,
    MatBadgeModule,
    NgbPaginationModule
  ],
  declarations: [
    LeadManagementComponent,
    LeadCreateComponent,
    HotelComponent,
    SightseeingComponent,
    TransfersComponent,
    RestaurantsComponent,
    LeadListComponent
  ]
})
export class LeadManagementModule { }
