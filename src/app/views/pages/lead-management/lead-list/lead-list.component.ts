import {Component, OnInit, ViewChild, ChangeDetectorRef} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { LeadCommonService } from '../../../../services/lead-common.service';
import { CommonService } from '../../../../services/common.service';
export interface UserData {
  id: string;
  name: string;
  progress: string;
  color: string;
}

/** Constants used to fill up our data base. */
const COLORS: string[] = [
  'maroon', 'red', 'orange', 'yellow', 'olive', 'green', 'purple', 'fuchsia', 'lime', 'teal',
  'aqua', 'blue', 'navy', 'black', 'gray'
];
const NAMES: string[] = [
  'Maia', 'Asher', 'Olivia', 'Atticus', 'Amelia', 'Jack', 'Charlotte', 'Theodore', 'Isla', 'Oliver',
  'Isabella', 'Jasper', 'Cora', 'Levi', 'Violet', 'Arthur', 'Mia', 'Thomas', 'Elizabeth'
];
@Component({
  selector: 'kt-lead-list',
  templateUrl: './lead-list.component.html',
  styleUrls: ['./lead-list.component.scss']
})
export class LeadListComponent implements OnInit {
  displayedColumns: string[] = ['Number',  'Custumer','Destination', 'Priority', 'Source', 'Date', 'Created', 'Pax'];
  dataSource: MatTableDataSource<UserData>;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    private cdr: ChangeDetectorRef,
    private commService: LeadCommonService,
  ) {
  //   // Create 100 users
  //   arrLeads : any[];
  //  // const users = Array.from({length: 100}, (_, k) => this.createNewUser(k + 1));

  //   // Assign the data to the data source for the table to render
  //   this.dataSource = new MatTableDataSource(arrLeads);
  }
  arrLeads : any[];
  ngOnInit() {
    debugger
    this.getLeads()
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
  getLeads() {
    this.commService.getLeads().subscribe((res:any) => {
      if(res.retCode==1){
        this.arrLeads =res.arrLeads;
        this.dataSource = new MatTableDataSource(this.arrLeads);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    
      this.cdr.detectChanges();
    })
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  /** Builds and returns a new User. */
 createNewLead(Lead: any): UserData {
   debugger
  return {
    id: Lead.Priority.toString(),
    name: Lead.Qoutation.CustomerType,
    progress: Math.round(Math.random() * 100).toString(),
    color: COLORS[Math.round(Math.random() * (COLORS.length - 1))]
  };
}
}

