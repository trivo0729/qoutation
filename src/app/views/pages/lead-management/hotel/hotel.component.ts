import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormArray, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import * as moment from 'moment';

import { Hotel, Rooms, child, objSearch, Destination } from '../../../../models/lead/hotel.model';
import { LeadCommonService } from '../../../../services/lead-common.service';
import { arrLead } from '../../../../models/lead/quotation.model';
import { hotel } from '../../../../models/lead/Qcommon.model';
import { LeadManagementComponent } from '../lead-management.component';
import { CommonService } from '../../../../services/common.service';
// Services
import { LayoutUtilsService, MessageType } from '../../../../core/_base/crud';

import { environment } from '../../../../../environments/environment';


@Component({
  selector: 'kt-hotel',
  templateUrl: './hotel.component.html',
  styleUrls: ['./hotel.component.scss']
})
export class HotelComponent implements OnInit {
  public hotelform: FormGroup;
  public RoomList: FormArray;
  price: any;
  hotelList: hotel[];
  lead: arrLead;
  objSearch: objSearch
  Trooms: any[];
  Rooms: Rooms[];
  hotel: Hotel[];
  child: child[];
  ages: any[];
  adults: any[];
  childs: any[];
  infants: any[];
  TGuest: number;
  Tguestselected: boolean;
  greater: boolean;
  show: boolean;
  selstart: any = moment('15-01-2020', 'DD-MM-YYYY');
  selend: any = moment('20-01-2020', 'DD-MM-YYYY');
  public daterange: any = {};
  public option: any;
  Response: boolean;
  loading: boolean;
  title: string;
  l: any;
  constructor(private cdr: ChangeDetectorRef,
    private leadComponent: LeadManagementComponent,
    private leadService: LeadCommonService,
    private common: CommonService,
    private layoutUtilsService: LayoutUtilsService,
    private router: Router,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.formInit();
    this.loading = false;
    this.l = 0
    this.TGuest = 0;
    this.hotelList = [];
    this.hotel = [];
    this.Trooms = [];
    this.ages = [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
    this.adults = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    this.childs = [0, 1, 2, 3, 4];
    this.infants = [0, 1, 2, 3, 4];
    this.child = [];
    this.show = false;
    this.Tguestselected = false;
    this.Response = false;
    this.right();
    this.getBasicDetails('lead');
    this.onMoreHotel();
    this.objSearch = new objSearch();
  }



  /* #region  form validation */
  formInit() {
    this.hotelform = this.fb.group({
      hotelname: [null, Validators.compose([Validators.required])],
      date: [null, Validators.compose([Validators.required])],
      rooms: this.fb.array([this.createRoomValid()])
    });
    // set contactlist to the form control containing contacts
    this.RoomList = this.hotelform.get('rooms') as FormArray;
  }

  createRoomValid(): FormGroup {
    return this.fb.group({
      roomtype: [null, Validators.compose([Validators.required])],
      roomrate: [null, Validators.compose([])],
      ebrate: [null, Validators.compose([])],
      cwbrate: [null, Validators.compose([])],
      cwobrate: [null, Validators.compose([])],
    });
  }

  updateRoomValid() {
    debugger;
    for (let i = 0; i < this.RoomList.controls.length; i++) {
      this.RoomList = this.hotelform.get('rooms') as FormArray;
      const formGroup = this.RoomList.controls[i] as FormGroup;

      formGroup.controls["roomtype"].setValidators(Validators.compose([Validators.required]));
      formGroup.controls["roomtype"].updateValueAndValidity();

      formGroup.controls["roomrate"].setValidators(Validators.compose([Validators.pattern("^[0-9]*$"), Validators.required]));
      formGroup.controls["roomrate"].updateValueAndValidity();

      formGroup.controls["ebrate"].setValidators(Validators.compose([Validators.pattern("^[0-9]*$"), Validators.required]));
      formGroup.controls["ebrate"].updateValueAndValidity();

      formGroup.controls["cwbrate"].setValidators(Validators.compose([Validators.pattern("^[0-9]*$"), Validators.required]));
      formGroup.controls["cwbrate"].updateValueAndValidity();

      formGroup.controls["cwobrate"].setValidators(Validators.compose([Validators.pattern("^[0-9]*$"), Validators.required]));
      formGroup.controls["cwobrate"].updateValueAndValidity();

    }
    console.log(this.RoomList);
  }


  // add a room form group
  addRoom() {
    this.RoomList.push(this.createRoomValid());
  }

  // remove room from group
  removeRoom(index) {
    this.RoomList.removeAt(index);
  }

  getRoomFormGroup(index): FormGroup {
    this.RoomList = this.hotelform.get('rooms') as FormArray;
    const formGroup = this.RoomList.controls[index] as FormGroup;
    return formGroup;
  }

  // get roomFormGroup() {
  //   return this.hotelform.get('rooms') as FormArray;
  // }
  /* #endregion form validation*/

  right() {
    this.leadComponent.onShowright(true);
  }

  onHotelelect(event: any) {
    this.objSearch.HotelCode = [event.id.split('|')[0]];
    this.objSearch.HotelName = event.value;
  }

  getBasicDetails(name: string) {
    this.lead = this.common.getBasicDetails(name);
    let i = this.lead.Queries.length - 1;
    let pax = this.lead.Queries[i].PaxDetails;
    this.TGuest = pax.Adult + pax.Child;
    this.setTotalRoom();
    this.setOptions(this.l);
  }

  setOptions(i: any) {
    this.option = {
      locale: { format: 'DD-MM-YYYY' },
      alwaysShowCalendars: false,
      showDropdowns: true,
      autoApply: true,
      startDate: moment(this.lead.Destination[i].Start, 'DD-MM-YYYY'),
      endDate: moment(this.lead.Destination[i].End, 'DD-MM-YYYY'),
      minDate: moment(this.lead.Destination[i].Start, 'DD-MM-YYYY'),
      maxDate: moment(this.lead.Destination[i].End, 'DD-MM-YYYY'),
    };
    this.title = `Hotels For ${this.lead.Destination[i].CityName}`;
  }

  setTotalRoom() {
    // for (let i = 0; i < this.Tadult; i++) {
    //   this.Trooms.push(i + 1)
    // }
  }

  // onRoomselect(index: number, h: any) {
  //   let data = this.hotel[h].Rooms;
  //   this.Rooms = [];
  //   if (index !== 1 && data.length < index) {
  //     for (let i = 0; i < index; i++) {
  //       if (i < data.length)
  //         this.Rooms.push({ no: i + 1, adult: data[i].adult, child: data[i].child, infant: data[i].infant, _child: [], price: 0, childwb: 0, childwob: 0, total: 0 });
  //       else
  //         this.Rooms.push({ no: i + 1, adult: 1, child: 0, infant: 0, _child: [], price: 0, childwb: 0, childwob: 0, total: 0 });
  //     }
  //   }
  //   else if (index !== 1 && data.length > index) {
  //     for (let i = 0; i < index; i++) {
  //       this.Rooms.push(
  //         { no: i + 1, adult: data[i].adult, child: data[i].child, infant: data[i].infant, _child: [], price: 0, childwb: 0, childwob: 0, total: 0 }
  //       )
  //     }
  //   }
  //   else if (index === 1 && data.length > index) {
  //     for (let i = 0; i < index; i++) {
  //       this.Rooms.push(
  //         { no: i + 1, adult: data[i].adult, child: data[i].child, infant: data[i].infant, _child: [], price: 0, childwb: 0, childwob: 0, total: 0 }
  //       )
  //     }
  //   }
  //   else {
  //     for (let i = 0; i < index; i++) {
  //       this.Rooms.push({ no: i + 1, adult: 1, child: 0, infant: 0, _child: this.child, price: 0, childwb: 0, childwob: 0, total: 0 });
  //     }
  //   }
  //   this.hotel[h].Rooms = this.Rooms;
  //   this.cdr.detectChanges();
  //   data = [];
  // }

  onChildselect(index: number, h: any, r: any) {
    debugger
    let data = this.hotel[h].Rooms[r]._child;
    this.child = [];
    for (let i = 0; i < index; i++) {
      this.child.push({
        no: i + 1,
        age: 2
      })
    }
    this.cdr.detectChanges();
    this.hotel[h].Rooms[r]._child = this.child;
    this.hotel[h].Rooms[r].child = index;
    this.TotalGuest(h);
    this.check(h);
  }

  onAdultelect(index: number, h: any, r: any) {
    this.TotalGuest(h);
    this.check(h)
  }

  onInfantselect(event: any, h: any, i: any) {
    this.hotel[h].Rooms[i].infant = event;
    //this.TotalGuest();
  }

  public selectedDate(value: any, datepicker?: any) {

    // any object can be passed to the selected event and it will be passed back here
    datepicker.start = value.start;
    datepicker.end = value.end;
    // or manupulat your own internal property
    this.daterange.start = value.start;
    this.daterange.end = value.end;
    this.daterange.label = value.label;
    this.lead.Destination[0].Start = moment(value.start).format('DD-MM-YYYY');
    this.lead.Destination[0].End = moment(value.end).format('DD-MM-YYYY');
    //this.check();
  }

  check(h: any) {
    // let leaddate = this.leadService.Getdays(this.selstart, this.selend);
    // let selecteddate = this.leadService.Getdays(this.daterange.start, this.daterange.end);
    // if (leaddate > selecteddate || !this.Tguestselected)
    //   this.show = true;
    // else
    //   this.show = false;
    // this.cdr.detectChanges();
    if (this.greater) {
      var msg = `can not select more than total travellers`
      this.notify(msg);
    }
  }

  TotalGuest(h: any) {
    this.Tguestselected = false;
    this.greater = false;
    let guest = 0;
    this.hotel[h].Rooms.forEach(r => {
      guest += r.adult;
      guest += r.child;
    })
    if (guest === this.TGuest)
      this.Tguestselected = true;
    else if (guest > this.TGuest) {
      this.greater = true;
      this.Tguestselected = true;
    }
    else {
      this.Tguestselected = false;
      this.greater = false;
    }
    return guest;
    //this.check()
  }

  getHotels(event: any, i: any) {
    let value = event.target.value;
    if (value.length < 3)
      return
    debugger
    let CityCode = this.lead.Destination[i].CityId;
    this.leadService.getHotelList(value, CityCode).subscribe((res: any) => {
      if (res.retCode === 1)
        this.hotelList = res.arrResult;
      this.cdr.detectChanges();
    })
  }

  onMoreHotel() {
    this.hotel.push({
      name: '',
      code: '',
      room: 1,
      mealname: '',
      mealcode: '',
      Rooms: []
    });
    let i = this.hotel.length - 1;
    this.hotel[i].Rooms = [];
    this.onAddRoom(i);
    //this.onRoomselect(1, i)
  }

  onAddRoom(h: any) {
    this.TotalGuest(h);
    if (this.Tguestselected)
      return
    this.hotel[h].Rooms.push(
      {
        no: 0, adult: 1, child: 0, infant: 0,
        _child: [], RateChildWithBed: 0,
        RateChildWithoutBed: 0, RoomRate: 0,
        RoomType: '',
        ExtraBedRate: 0, Total: 0, Currency: '',
        description: [],
        rateAvailable: true,
        response: false,
        loading: false
      });
    this.TotalGuest(h);
    this.cdr.detectChanges();

  }

  notify(Message: string) {
    this.layoutUtilsService.showActionNotification(Message, MessageType.Delete);
  }

  onRemoveRoom(h: any, i: any) {
    this.hotel[h].Rooms.splice(i, 1);
    this.TotalGuest(h);
    this.cdr.detectChanges();
  }

  onSearch(index: any) {
    debugger
    if (this.greater) {
      var msg = `can not select more than total travellers`
      this.notify(msg);
      return
    }

    this.loading = true;
    this.objSearch.Destination = new Destination
    this.objSearch.Destination.City = this.lead.Destination[index].CityName;
    this.objSearch.Destination.Code = this.lead.Destination[index].CityId;
    this.objSearch.Destination.Country = this.lead.Destination[index].Country;
    this.objSearch.Checkin = this.lead.Destination[index].Start;
    this.objSearch.Checkout = this.lead.Destination[index].End;
    this.objSearch.Nights = 0;
    let nationality = this.common.getCountry(this.lead.GuestNationality);
    this.objSearch.nationality = nationality.name;
    this.objSearch.username = environment.UserName;
    this.objSearch.Password = environment.Password;
    this.objSearch.Supplier = "";
    this.objSearch.MealPlan = "";
    this.objSearch.CurrencyCode = "USD";
    this.objSearch.SearchType = "";
    this.objSearch.TockenID = '';
    this.objSearch.UserIP = '';
    for (let i = 0; i < this.hotel[0].Rooms.length; i++) {
      this.objSearch.Rooms = [];
      this.objSearch.Adults = 0;
      this.objSearch.Childs = 0;
      let Requestno = 0;
      let Occupancy = [];
      var ChildAges = []
      if (this.hotel[0].Rooms[i]._child.length != 0) {
        for (let j = 0; j < this.hotel[0].Rooms[i]._child.length; j++) {
          ChildAges.push(this.hotel[0].Rooms[i]._child[j].age)
        }
      }
      Occupancy.push({ RoomCount: i + 1, AdultCount: this.hotel[0].Rooms[i].adult, ChildCount: this.hotel[0].Rooms[i].child, ChildAges: ChildAges });
      this.objSearch.Adults += this.hotel[0].Rooms[i].adult;
      this.objSearch.Childs += this.hotel[0].Rooms[i].child;
      this.objSearch.Rooms = Occupancy;
      Requestno = i + 1;
      this.leadService.getHotels(this.objSearch, Requestno).subscribe((result) => {
        this.loading = false;
        this.Response = true;
        let Reqno = result.Requestno - 1;
        this.hotel[0].Rooms[Reqno].response = true;
        if (result.retCode === 1) {
          this.hotel[0].Rooms[Reqno].description = result.arrResult;
          if (result.arrResult.length == 0)
            this.hotel[0].Rooms[Reqno].rateAvailable = false;
          else
            this.hotel[0].Rooms[Reqno].rateAvailable = true;
        }
        this.cdr.detectChanges();
      })
    }
  }

  Modify() {
    this.Response = false;
  }

  getRates() {

  }

  onroomtypeSelect(h: any, i: any, event: any) {
    debugger;
    this.hotel[h].Rooms[i].RoomRate = event.RoomRate.toFixed(2);
    this.hotel[h].Rooms[i].ExtraBedRate = event.ExtraBedRate.toFixed(2);
    this.hotel[h].Rooms[i].RateChildWithBed = event.RateChildWithBed.toFixed(2);
    this.hotel[h].Rooms[i].RateChildWithoutBed = event.RateChildWithoutBed.toFixed(2);
    this.hotel[h].Rooms[i].Total = event.Total.toFixed(2);
    console.log(this.hotel);
    this.cdr.detectChanges();
  }

  onSkip(i: any) {
    if (this.lead.Destination.length > i + 1) {
      this.l++
      this.setOptions(this.l);
      this.Response = false;
    }
    else
      this.router.navigate(['/lead-management/sightseeing'])

  }

  onAddToQuote(h: any, l: any) {
    debugger
    this.leadComponent.makesummary(this.objSearch, 'Hotel', this.hotel[h].Rooms, l);
    if (this.lead.Destination.length > l + 1) {
      this.l++
      this.setOptions(this.l);
    }
    this.Response = false;
  }

}
