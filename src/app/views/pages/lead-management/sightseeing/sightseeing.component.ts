import { Component, OnInit, ChangeDetectorRef, HostListener } from '@angular/core';
import { Search_Params, Tours, Slots, TourType, TicketType } from '../../../../models/lead/sightseeing';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToursService } from '../../../../services/sightseeing.service';
import { LeadManagementComponent } from '../lead-management.component';
import { arrLead } from '../../../../models/lead/quotation.model';
import { CommonService } from '../../../../services/common.service';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgbPopover } from '@ng-bootstrap/ng-bootstrap';
import { NgbPopoverConfig } from '@ng-bootstrap/ng-bootstrap';
import { MatCheckboxChange } from '@angular/material';
@Component({
  selector: 'kt-sightseeing',
  templateUrl: './sightseeing.component.html',
  styleUrls: ['./sightseeing.component.scss'],
  providers: [NgbPopoverConfig]
})
export class SightseeingComponent implements OnInit {

  constructor(
    private fb: FormBuilder,
    private tourService: ToursService,
    private cdr: ChangeDetectorRef,
    private leadComponent: LeadManagementComponent,
    private common: CommonService,
    private router: Router,
    private modalService: NgbModal,
    config: NgbPopoverConfig
  ) { config.container = 'body'; }
  arrSearch: Search_Params = new Search_Params();
  lead: arrLead;
  show: boolean;
  mouse_is_inside: boolean = false;
  TourForm: FormGroup;
  location: string = "";
  i: number = 0;
  Date: string = "18-01-2020";
  slotID: number;
  ratetype: string;
  arrTours: Tours[];
  arrRates: TicketType[];
  arrTheme: any[];
  arrSightseeing: any[];
  bloder:boolean=false;
  ID:Number;
  @HostListener('window:mouseup', ['$event'])
  onMouseUp() {
    if (!this.mouse_is_inside){
      this.arrTours.find(d=>d.ID == this.ID).show= false;
    }
  }

  ngOnInit() {
    this.right();
    
    this.arrSearch = new Search_Params();
    this.lead = this.common.getBasicDetails("lead");
    if (this.lead != undefined && this.lead.Destination.length != 0)
      this.location = this.lead.Destination[this.i].CityName;
    this.search();
  }
  initform() {
    this.TourForm = this.fb.group({
      Desination: ['', Validators.compose([Validators.required])]
    });
  }
  onShow(ID:Number) {
   this.arrTours.find(d=>d.ID == ID).show= true;
    this.cdr.detectChanges();
  }
  onMouseover(event: boolean,ID:number) {
    this.mouse_is_inside = event;
    this.ID = ID;
  }

  _skipSightseeing() {
    debugger;
    if ((this.lead.Destination.length - 1) >= this.i) {
      this.i++;
      this.location = this.lead.Destination[this.i].CityName;
      this.arrTours = [];
      this.search();
      this.cdr.detectChanges();
    }
    else {
      this.router.navigate(["/lead-management/transfers"])
    }
  }


  getComponentTitle() {
    let result = 'Sightseeing';
    if (!this.location) {
      return result;
    }
    result = `Sightseeing for ${this.location}`;
    return result;
  }

  getTours(event: any) {
    let value = event.target.value;
    if (value.length < 3)
      return
    debugger
    this.tourService.GetTourName(value).subscribe((res: any) => {
      if (res.retCode === 1)
        this.arrSightseeing = res.arrTours;
      this.cdr.detectChanges();
    })
  }

  search() {
    debugger;
    this.bloder = true;
    this.arrTours = [];
    this.arrSearch = new Search_Params();
    this.location = this.lead.Destination[this.i].CityName;
    this.arrSearch.LocationName = this.location;
    this.arrSearch.sCurrency =this.lead.Currency; 
    this.Date = this.lead.Destination[this.i].Start;
    var arrSelected = this.leadComponent._getSelectedTour(this.i);
    this.tourService.getTours(this.location, this.Date, '',this.arrSearch.sCurrency ).subscribe((res: any) => {
      if (res.retCode == 1) {
        this.bloder = false;
        this.arrTours = res.arrTour;
        this.arrTours.forEach(d => {
          d.arrSelRates = d.arrRates.filter(r => r.SlotID == d.arrRates[0].SlotID);
          d.selectedTicket = d.arrSelRates[0];
          d.selectedTicket.arrayPax.forEach(p => {
            if (p.Pax_Type == "adult") {
              p.Count = 1;
              d.selectedTicket.Adult = 1;
            } else if (p.Pax_Type == "child1") {
              p.Count = 0;
              d.selectedTicket.Child1 = 0;
            } else if (p.Pax_Type == "child2") {
              p.Count = 0;
              d.selectedTicket.Child2 = 0;
            } else if (p.Pax_Type == "infant") {
              p.Count = 0;
              d.selectedTicket.Infant = 0;
            }
          });
          d.selSlot =  d.selectedTicket.SlotID;
          d.selRateType = d.selectedTicket.RateType;
          d.selTicket = d.selectedTicket.Ticket;
          d.Price = d.selectedTicket.Currency + ' ' + d.selectedTicket.arrayPax.find(d => d.Pax_Type == "adult").Rate;
          d.bselected = arrSelected.includes(d.ID);
          d.show = false;
        });
        this.cdr.detectChanges();
      }
    });
  }



  right() {
    this.leadComponent.onShowright(true);
  }
  _getTickets(TourID: Number) {

  }

  _getTicketBySlot(ID: Number) {
    var arrTour = this.arrTours.find(d => d.ID == ID);
    this.arrTours.find(d => d.ID == ID).Price = arrTour.arrSelRates[0].Currency + ' ' + arrTour.arrSelRates.find(d => d.Ticket == arrTour.selTicket).arrayPax.find(d => d.Pax_Type == "adult").Rate;
    this.cdr.detectChanges();
  }
  _getSlotRate(ID: Number) {
    debugger
    var arrTour = this.arrTours.find(d => d.ID == ID);
    arrTour.arrSelRates = arrTour.arrRates.filter(d => d.SlotID == arrTour.selSlot && d.RateType == arrTour.selRateType);
    this.arrTours.find(d => d.ID == ID).selectedTicket = arrTour.arrSelRates[0];
    this.arrTours.find(d => d.ID == ID).selectedTicket.arrayPax.forEach(p => {
      if (p.Pax_Type == "adult") {
        p.Count = 1;
        this.arrTours.find(d => d.ID == ID).selectedTicket.Adult = 1;
      } else if (p.Pax_Type == "child1") {
        p.Count = 0;
        this.arrTours.find(d => d.ID == ID).selectedTicket.Child1 = 0;
      } else if (p.Pax_Type == "child2") {
        p.Count = 0;
        this.arrTours.find(d => d.ID == ID).selectedTicket.Child2 = 0;
      } else if (p.Pax_Type == "infant") {
        p.Count = 0;
        this.arrTours.find(d => d.ID == ID).selectedTicket.Infant = 0;
      }
    });
    this.arrTours.find(d => d.ID == ID).selSlot = arrTour.arrSelRates[0].SlotID;
    this.arrTours.find(d => d.ID == ID).selRateType = arrTour.arrSelRates[0].RateType;
    this.arrTours.find(d => d.ID == ID).selTicket = arrTour.arrSelRates[0].Ticket;
    this.arrTours.find(d => d.ID == ID).Price = arrTour.arrSelRates[0].Currency + ' ' + arrTour.arrSelRates[0].arrayPax.find(d => d.Pax_Type == "adult").Rate;
    this.cdr.detectChanges();
  }
  addPax(paxType: any,ID:Number) {
    debugger;
    this.arrTours.find(d=>d.ID == ID).selectedTicket.arrayPax.find(p=>p.Pax_Type ==paxType ).Count++; 
    if(paxType =="adult")
      this.arrTours.find(d=>d.ID == ID).selectedTicket.Adult ++;
      if(paxType =="child1" )
        this.arrTours.find(d=>d.ID == ID).selectedTicket.Child1 ++;
        if( paxType =="child2" )
        this.arrTours.find(d=>d.ID == ID).selectedTicket.Child1 ++;
        if( paxType =="infant" )
        this.arrTours.find(d=>d.ID == ID).selectedTicket.Infant ++;
    this.cdr.detectChanges();
  }
 removePax(paxType: any,ID:Number) {
    debugger;
    this.arrTours.find(d=>d.ID == ID).selectedTicket.arrayPax.find(p=>p.Pax_Type ==paxType ).Count--; 
    if(paxType =="adult")
      this.arrTours.find(d=>d.ID == ID).selectedTicket.Adult --;
      if(paxType =="child1" )
        this.arrTours.find(d=>d.ID == ID).selectedTicket.Child1 --;
        if( paxType =="child2" )
        this.arrTours.find(d=>d.ID == ID).selectedTicket.Child1 --;
        if( paxType =="infant" )
        this.arrTours.find(d=>d.ID == ID).selectedTicket.Infant --;
    this.cdr.detectChanges();
  }
  _validatePax(PaxType:string,Count:number,ID:Number){
    var bValid = true;
    var arrTour =this.arrTours.find(d=>d.ID == ID);
    try{

    }catch(ex){console.log(ex)}
  }

  _add(event: MatCheckboxChange,ID:Number) {
    debugger;
    try {
      var arrTour = this.arrTours.find(d => d.ID == ID);
      if (event.checked)
      {
        this.leadComponent.makesummary(arrTour, 'Sightseeing', this.lead.Destination[this.i].CityId, this.i);
      }
      else
        this.leadComponent.tourRemove(ID, this.lead.Destination[this.i].CityId, this.i);
    } catch (e) {
      console.log(e)
    }
  }

  /**
	 * Checking control validation
	 *
	 * @param controlName: string => Equals to formControlName
	 * @param validationType: string => Equals to valitors name
	 */
  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.TourForm.controls[controlName];
    if (!control) {
      return false;
    }
    const result = control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }

}
