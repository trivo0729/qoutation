import { Component, OnInit, ChangeDetectorRef, HostListener } from '@angular/core';
// Layout
import { SubheaderService } from '../../../../../core/_base/layout';
import { FormControl, FormGroup, FormBuilder, Validators, FormArray, ValidationErrors } from '@angular/forms';

import * as moment from 'moment';
import { LeadCommonService } from '../../../../../services/lead-common.service';
import { PaxDetails, arrLead, locations } from '../../../../../models/lead/quotation.model';
import { User, Staff, arrCurrency } from '../../../../../models/lead/Qcommon.model';
import { Router } from '@angular/router';
import { LeadManagementComponent } from '../../lead-management.component';
import { environment } from '../../../../../../environments/environment';
import { CommonService } from '../../../../../services/common.service';

@Component({
  selector: 'kt-lead-create',
  templateUrl: './lead-create.component.html',
  styleUrls: ['./lead-create.component.scss']
})
export class LeadCreateComponent implements OnInit {

  /**
	 * Component constructor
	 *
	 * @param subheaderService: SubheaderService
	 */

  @HostListener('window:mouseup', ['$event'])
  onMouseUp() {
    if (!this.mouse_is_inside)
      this.show = false;
  }

  // Public params
  display: boolean;
  leadForm: FormGroup;
  public Destinationlist: FormArray;
  itemList: any = [];
  settings = {};
  filteredOptions: User[];
  userControl = new FormControl();
  loadingAfterSubmit: boolean;
  viewLoading: boolean;
  users: User[];
  staff: Staff[];
  minDate: any;
  files: File[] = [];
  show: boolean;
  mouse_is_inside: boolean = false;
  public daterange: any = {};
  public option: any = {
    locale: { format: 'DD-MM-YYYY' },
    alwaysShowCalendars: false,
    showDropdowns: true,
    autoApply: true,
    minDate: moment(new Date())
  };

  Nationality: any[];
  pax: PaxDetails;
  lead: arrLead;
  arrCurrency: arrCurrency[]
  Totalguest: string;
  customer: string;
  assign: string;
  Destination: locations[];
  DefaultCurrency: string;

  constructor(private subheaderService: SubheaderService,
    private cdr: ChangeDetectorRef,
    private commService: LeadCommonService,
    private fb: FormBuilder,
    private router: Router,
    private common: CommonService,
    private leadComponent: LeadManagementComponent,
  ) {
  }

  ngOnInit() {

    this.loadingAfterSubmit = false;
    this.viewLoading = false;
    this.Destination = [];
    this.arrCurrency = [];
    this.staff = [];
    this.pax = new PaxDetails();
    this.lead = new arrLead();
    this.lead.Queries = [];
    this.lead.Destination = [];
    this.pax.Adult = 1;
    this.pax.Child = 0;
    this.pax.Infant = 0;
    this.Nationality = [];
    this.lead.Source = 'Email';
    this.lead.CustomerType = 'B2B';
    this.display = false;
    this.settings = {
      text: "Select Destinations",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      classes: "myclass custom-class",
      singleSelection: true,
      primaryKey: "value",
      labelKey: "value",
      noDataLabel: "Search Destinations...",
      enableSearchFilter: true,
      badgeShowLimit: 1,
      searchBy: ['value', 'value']
    };

    this.initLead();
    this.show = false;
    var min = new Date();
    this.minDate = moment(min);
    this.getUsers();
    this.initform();
    this.onPushDestination();
    this.validation('B2B');
    this.getNationality();
    this.totalTraveller();
    this.getStaff();
    this.right();
    this.getCurrency();
  }

  right() {
    this.leadComponent.onShowright(false);
  }

  getNationality() {
    this.commService.getCountry().subscribe((res) => {
      this.Nationality = res;
      this.cdr.detectChanges();
    })
  }

  getUsers() {
    this.commService.getAgents().subscribe((res: any) => {
      if (res.retCode === 1) {
        this.users = res.arrResult;
      }
      else
        alert();
    });
  }

  getCurrency() {
    this.common.GetCurrency().subscribe((res: any) => {
      if (res.retCode === 1)
        this.arrCurrency = res.arrCurrency;
      this.cdr.detectChanges();
      this.DefaultCurrency = 'USD';
    })
  }

  OnsearchUser(event: any) {
    let value = event.target.value;
    if (value.length < 3)
      return
    this.filteredOptions = this.users.filter(u => u.name.toLocaleLowerCase().includes((value).toLocaleLowerCase()))
  }

  getStaff() {
    debugger
    this.commService.getStaff().subscribe((res: any) => {
      if (res.retCode === 1)
        this.staff = res.arrResult;
      this.cdr.detectChanges();
    })
  }

  onPushDestination() {
    debugger
    let min = moment(new Date())
    if (this.Destination.length > 0) {
      if (this.leadForm.invalid) {
        const controls = this.leadForm.controls;
        Object.keys(controls).forEach(controlName =>
          controls[controlName].markAsTouched()
        );
        return;
      }
      let i = this.Destination.length - 1;
      let date = this.Destination[i].Date.split(' - ')[1].trim()
      min = moment(date, 'DD-MM-YYYY');
    }
    this.Destination.push({
      selectedItems: [],
      Date: '',
      option: {
        locale: { format: 'DD-MM-YYYY' },
        alwaysShowCalendars: false,
        showDropdowns: true,
        autoApply: true,
        minDate: min
      }
    });
    if (this.Destination.length > 1)
      this.addvalid();
  }

  initLead() {
    this.subheaderService.setTitle('Create lead');
  }

  initform() {
    this.leadForm = this.fb.group({
      customer: ['', Validators.compose([])],
      customername: ['', Validators.compose([])],
      title: ['', Validators.compose([])],
      email: ['', Validators.compose([])],
      mobile: ['', Validators.compose([])],
      nationality: ['', Validators.compose([Validators.required])],
      traveller: ['', Validators.compose([Validators.required])],
      adult: ['', Validators.compose([])],
      child: ['', Validators.compose([])],
      infant: ['', Validators.compose([])],
      laedingpax: ['', Validators.compose([])],
      puspose: ['', Validators.compose([])],
      source: ['', Validators.compose([])],
      query: ['', Validators.compose([])],
      assign: ['', Validators.compose([Validators.required])],
      currency: ['', Validators.compose([Validators.required])],
      d_list: this.fb.array([this.create()])
    });
    this.Destinationlist = this.leadForm.get('d_list') as FormArray;
  }

  onuser(event: any) {
    debugger
    if (event.value === 'B2B') {
      this.display = false;
      this.lead.CustomerType = event.value;
      this.validation(event.value);
    }
    else {
      this.display = true;
      this.lead.CustomerType = event.value;
      this.validation(event.value);
    }
  }

  onUserselect(value) {
    this.lead.CustomerId = value.sid;
  }

  validation(user: any) {
    if (user === 'B2B') {
      let validators = Validators.compose([Validators.required]);
      this.leadForm.controls["customer"].setValidators(validators);
      this.leadForm.controls["customer"].updateValueAndValidity();
      this.leadForm.controls["customername"].clearValidators();
      this.leadForm.controls["customername"].updateValueAndValidity();

      this.leadForm.controls["mobile"].clearValidators();
      this.leadForm.controls["mobile"].updateValueAndValidity();
      this.leadForm.controls["email"].clearValidators();
      this.leadForm.controls["email"].updateValueAndValidity();
    }

    else {
      let validators = Validators.compose([Validators.required]);
      this.leadForm.controls["customername"].setValidators(validators);
      this.leadForm.controls["customername"].updateValueAndValidity();
      this.leadForm.controls["customer"].clearValidators();
      this.leadForm.controls["customer"].updateValueAndValidity();

      let mobile = Validators.compose([Validators.pattern("^[0-9]*$"), Validators.required]);
      this.leadForm.controls["mobile"].setValidators(mobile);
      this.leadForm.controls["mobile"].updateValueAndValidity();

      let email = Validators.compose([Validators.email, Validators.required]);
      this.leadForm.controls["email"].setValidators(email);
      this.leadForm.controls["email"].updateValueAndValidity();
    }

  }

  create(): FormGroup {
    return this.fb.group({
      destination: ['', Validators.compose([Validators.required])],
      date: ['', Validators.compose([Validators.required])],
    });
  }

  updateValidation(value: any) {
    if (value !== "email") {
      this.leadForm.controls["email"].clearValidators();
      this.leadForm.controls["email"].updateValueAndValidity();
    }

    else if (value !== "mobile") {
      this.leadForm.controls["mobile"].clearValidators();
      this.leadForm.controls["mobile"].updateValueAndValidity();
    }

  }

  checkValid() {
    debugger
    if (this.leadForm.get('mobile').valid) {
      this.updateValidation('mobile');
    } else if (this.leadForm.get('email').valid) {
      this.updateValidation('email');
    }

    const controls = this.leadForm.controls;
    if ((controls.email.value === '' || controls.email.value === undefined) && (controls.mobile.value === '' || controls.mobile.value === undefined)) {
      this.validation('B2C')
    }
  }

  addvalid() {
    this.Destinationlist.push(this.create());
  }

  getFormGroup(i: any): FormGroup {
    const formGroup = this.Destinationlist.controls[i] as FormGroup;
    return formGroup;
  }

  // getFormGroup() {
  //   return this.leadForm.get('d_list') as FormArray;
  // }

  onsubmit() {
    debugger
    const controls = this.leadForm.controls;
    /** check form */
    if (this.leadForm.invalid) {
      Object.keys(controls).forEach(controlName =>
        controls[controlName].markAsTouched()
      );
      return;
    }
    this.lead.Destination = [];
    this.lead.Queries = [];
    this.viewLoading = true;
    this.lead.FileNo = '';
    this.lead.ParentId = environment.AdminID;
    this.lead.CreateDate = '';
    // this.lead.CustomerID = controls.customer.value;
    if (this.lead.CustomerType === 'B2B')
      this.lead.PackageTitle = controls.title.value;
    else {
      this.lead.PackageTitle = controls.customername.value;
      //this.lead.CustomerId = controls.customername.value;
      this.lead.Email = controls.email.value;
      this.lead.Mobile = controls.mobile.value;
    }
    this.lead.PurposeVist = controls.puspose.value;
    this.lead.Query = controls.query.value;
    this.lead.CreateBy = '';
    this.lead.Currency = controls.currency.value;
    this.lead.GuestNationality = controls.nationality.value;
    this.lead.FollowUp = [];
    this.lead.FollowUp.push(
      {
        NextDate: '',
        LastDate: '',
        By: {
          User: '',
          On: ''
        }
      }
    );

    this.Destination.forEach(d => {
      this.lead.Destination.push(
        {
          CityId: d.selectedItems[0].id,
          CityName: d.selectedItems[0].value.split(',')[0],
          Country: d.selectedItems[0].value.split(',')[1].trim(),
          State: '',
          Start: d.Date.split(' - ')[0].trim(),
          End: d.Date.split(' - ')[1].trim(),
        }
      );
    });
    let Qoute = {
      Discription: '',
      Location: []
    }
    this.lead.Queries.push(
      {
        QueryNo: '',
        Handler: controls.assign.value,
        Date: '',
        StartDate: '',
        EndDate: '',
        PaxDetails: this.pax,
        Destination: this.lead.Destination,
        Status: '',
        Qoute: Qoute,
      }
    );
    this.Onsave();
  }

  Onsave() {
    debugger
    console.log(JSON.stringify(this.lead));
    this.commService.saveQoutation(this.lead).subscribe((result: any) => {
      this.viewLoading = false;
      localStorage.setItem('lead', JSON.stringify(result));
      if (result.retCode === 1) {
        this.leadComponent.getBasicDetails('lead');
        window.location.href = 'lead-management/hotel';
        //this.router.navigate(['/lead-management/hotel']);
      }
      else
        alert('error msg')
    })
  }

  onclick() {

  }

  onSelect(event) {
    console.log(event);
    this.files.push(...event.addedFiles);
  }

  onnationality(event: any) {
    //this.lead.GuestNationality.code = event;
  }

  onRemove(event) {
    this.Destination.splice(this.Destination.indexOf(event), 1);
    this.Destinationlist.removeAt(event);
  }

  onShow() {
    if (this.show)
      this.show = false;
    else
      this.show = true;
    this.cdr.detectChanges();
  }
  onMouseover(event: boolean) {
    this.mouse_is_inside = event;
  }

  public selectedDate(value: any, datepicker?: any) {
    debugger
    this.Destination[datepicker].Date = moment(value.start).format('DD-MM-YYYY') + ' - ' + moment(value.end).format('DD-MM-YYYY');
    this.cdr.detectChanges();
  }

  onSearch(evt: any) {
    this.itemList = [];
    let value = evt.target.value
    if (value.length < 3)
      return;
    this.commService.getHotelLocation(evt.target.value).subscribe((res) => {
      debugger
      this.itemList = res;
      this.cdr.detectChanges();
    });

  }

  onItemSelect(item: any) {

  }
  OnItemDeSelect(item: any) {
  }
  onSelectAll(items: any) {

  }
  onDeSelectAll(items: any) {
  }

  add(paxType: any) {
    switch (paxType) {
      case 'ad':
        this.pax.Adult = this.pax.Adult + 1;
        break;
      case 'ch':
        this.pax.Child = this.pax.Child + 1;
        break;
      case 'in':
        this.pax.Infant = this.pax.Infant + 1;
        break
    }
    this.totalTraveller();
  }

  remove(paxType: any) {
    switch (paxType) {
      case 'ad':
        if (this.pax.Adult > 1)
          this.pax.Adult = this.pax.Adult - 1;
        break;
      case 'ch':
        if (this.pax.Child > 0)
          this.pax.Child = this.pax.Child - 1;
        break;
      case 'in':
        if (this.pax.Infant > 0)
          this.pax.Infant = this.pax.Infant - 1;
        break
    }
    this.totalTraveller();
  }

  totalTraveller() {
    this.Totalguest = `${this.pax.Adult} Adult, ${this.pax.Child} Child, ${this.pax.Infant} Infant`
  }

  /**
	 * Checking control validation
	 *
	 * @param controlName: string => Equals to formControlName
	 * @param validationType: string => Equals to valitors name
	 */
  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.leadForm.controls[controlName];
    if (!control) {
      return false;
    }
    const result = control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }

  // isFormArrayHasError(controlName: string, validationType: string, i: any): boolean {
  //   debugger
  //   const arr = this.leadForm.get('d_list') as FormArray;
  //   const formGroup = arr.controls[i] as FormGroup;
  //   const control = formGroup.controls[controlName];
  //   if (!control) {
  //     return false;
  //   }
  //   const result = control.hasError(validationType) && (control.dirty || control.touched);
  //   return result;
  // }



}

