import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { arrLead } from '../../../models/lead/quotation.model';
import { CommonService } from '../../../services/common.service';
import { summary, locations, service, Hotel, Rooms, child } from '../../../models/lead/summary.model';

@Component({
  selector: 'kt-lead-management',
  templateUrl: './lead-management.component.html',
  styleUrls: ['./lead-management.component.scss']
})
export class LeadManagementComponent implements OnInit {
  lead: arrLead;
  summary: summary;
  service: service[]
  Hotel: Hotel[];
  Tour: any[];
  Transfer:any[];
  Resturant:any[];
  location: locations[];
  Rooms: Rooms[];
  showright: boolean;
  panelOpenState: boolean = true;
  step = 0;
  skip: boolean;
  arrSummary : summary;
  bloder : boolean = false;
  constructor(private common: CommonService,
    private cdr: ChangeDetectorRef
  ) { }

  ngOnInit() {
    this.skip = false;
    this.lead = new arrLead();
    this.summary = new summary();
    this.lead.Destination = [];
    this.summary.location = [];
    this.location = [];
    this.Tour = [];
    this.onShowright(false);
    this.getBasicDetails('lead');
    this. getSummary();

  }

  onShowright(show: boolean) {
    this.showright = show;
    this.cdr.detectChanges();
  }

  getSummary(){
    try{
      debugger
      this.bloder = true;
      var   FileNo = this.summary.FileNo;
      var QueryNo = this.summary.QueryNo;
      this.common.GetSummary(FileNo,QueryNo).subscribe((res: any) => {
        this.bloder = false;
        if (res.retCode === 1) {
           var Qoute  = res.arrData.Queries.find(d=>d.QueryNo == QueryNo).Qoute;
           this.summary.location .forEach((l,i)=>{
              var arrLocation = Qoute.Location.find(d=>d.LocationCode == l.LocationCode)
               if(arrLocation != undefined){
                l.Service = arrLocation.Service;
                l.Service.forEach(d=>{
                  try{
                    d.Transfer.forEach(d=>{
                      d.data = JSON.parse(d.Data);
                    })
                  }catch(e){
                    d.Transfer.forEach(d=>{
                      d.data = JSON.parse(JSON.parse(d.Data));
                    })
                  }
                    
                })
               }
              this._getLocationTotal(i);
           });
        }
        else
          alert();
      });
    }catch(e){}
  }

  getBasicDetails(name: string) {
    debugger
    this.lead = this.common.getBasicDetails(name);
    this.location = [];
    for (const d of this.lead.Destination) {
      this.location.push(
        {
          Name: d.CityName,
          LocationCode: d.CityId,
          Service: [{ Hotel: [], Tour: [] ,Transfer:[], Restaurant:[] }],
          price: 0.00
        }
      );
    }
    let q = this.lead.Queries.length - 1;
    this.summary.FileNo = this.lead.FileNo;
    this.summary.QueryNo = this.lead.Queries[q].QueryNo;
    this.summary.location = this.location;
    this.cdr.detectChanges();
  }

  setStep(index: number) {
    this.step = index;
  }

  makesummary(data: any, service: any, _room: any, i: any) {
    debugger

    if (service === "Hotel")
      this.hotelsummary(data, _room, i)
    else if (service === "Sightseeing")
      this.toursummary(data, _room, i)
    else if (service === "Transfer")
      this.transferSummary(data, _room, i)
      else if (service === "Restaurant")
      this.restSummary(data, _room, i)
    this.cdr.detectChanges();
  }

  hotelsummary(data: any, _room: any, i: any) {
    var  service = [];
    this.arrSummary = new summary();
    this.Hotel = [];
    this.Rooms = [];
    let shortinfo;
    let adult = 0;
    let child = 0;
    let infant = 0;
    let age = '';
    let total = 0;

    for (let r = 0; r < _room.length; r++) {
      let ages = "";
      for (let c = 0; c < _room[r]._child.length; c++) {
        if (c !== _room[r]._child.length - 1) {
          ages += `${_room[r]._child[c].age}, `;
          age += `${_room[r]._child[c].age} yrs, `;
        }
        else {
          ages += `${_room[r]._child[c].age}`;
          age += `${_room[r]._child[c].age} yrs`
        }
      }
      adult += _room[r].adult;
      child += _room[r].child;
      this.Rooms.push({
        no: r + 1, adult: _room[r].adult,
        child: _room[r].child, infant: 0, _child: ages, price: _room[r].total,
        RoomType: _room[r].RoomType,
        RateChildWithBed: _room[r].RateChildWithBed,
        RateChildWithoutBed: _room[r].RateChildWithoutBed,
        RoomRate: _room[r].RoomRate,
        ExtraBedRate: _room[r].ExtraBedRate,
        Total: _room[r].Total,
        Currency: _room[r].Currency,
      });
      total += parseFloat(_room[r].Total);
    }

    shortinfo = `${this.Rooms.length}  Rooms  ${adult}  Adult  ${child}  Child  (${age})  ${infant}  Infant`
    this.Hotel.push(
      {
        name: data.HotelName,
        checkin: data.Checkin,
        checkout: data.Checkout,
        code: data.HotelCode[0],
        room: this.Rooms.length,
        mealname: '',
        mealcode: '',
        Rooms: this.Rooms,
        shortinfo: shortinfo,
        total: total
      }
    )
     service.push({ Hotel: this.Hotel, Tour: [], Transfer:[]});
     this.arrSummary.location =[];
     this.arrSummary.location.push(this.summary.location[i]);
     this.arrSummary.location[0].Service = service;
     this.onSummarySave(this.arrSummary.location);
  }

  toursummary(data: any, _location: any, i: any) {
    var  service = [];
    var tour =[];
    this.arrSummary = new summary();
    if (this.Tour == undefined || this.Tour.filter(d => d.location == _location).length == 0)
      this.Tour = [];
    let shortinfo;
    let adult = 0;
    let child = 0;
    let infant = 0;
    let age = '';
    let total = 0;
    data.selectedTicket.arrayPax.forEach(d => {
      total += parseFloat(d.Rate) * d.Count;
      if (d.Pax_Type == "adult")
        adult = d.Count;
      else if (d.Pax_Type == "child1")
        child += d.Count;
      else if (d.Pax_Type == "child2")
        child += d.Count;
      else if (d.Pax_Type == "infant")
        infant += d.Count;
    });

    shortinfo = `${data.selTicket}  (${data.selRateType})  ${adult}  Adult  
    ${child}  Child  (${age})  ${infant}  Infant`
    tour.push(
      {
        Name: data.Name,
        Date: '',
        Code: data.ID,
        Location: _location,
        Shortinfo: shortinfo,
        Data: JSON.stringify(data) ,
        Total: total.toFixed(2)
      }
    )
     service.push({ Hotel: [], Tour: tour ,Transfer:[]});
     this.arrSummary.location =[];
     this.arrSummary.location.push(this.summary.location[i]);
     this.arrSummary.location[0].Service = service;
     this.onSummarySave(this.arrSummary.location);
  }

  transferSummary(data: any, _location: any, i: any) {
    debugger
    var  service = [];
    var transferummary =[];
    this.arrSummary = new summary();
    this.Transfer = [];
    let total = 0;
    this.Transfer.push(
      {
        PickupFrom: data.PickupFrom,
        DropOn: data.DropOn,
        VehicleModle: data.VehicleModle,
        Date: '',
        Trip: data.Trip + ' ' + data.ServiceType,
        Data: JSON.stringify(data) ,
        Total: data.TotalCharge,
      }
    )
     service.push({ Hotel: [], Tour: [], Transfer:this.Transfer});
     this.arrSummary.location =[];
     this.arrSummary.location.push(this.summary.location[i]);
     this.arrSummary.location[0].Service = service;
     this.onSummarySave(this.arrSummary.location);
  }

  restSummary(data: any, _location: any, i: any) {
    debugger
    var  service = [];
    var restSummary =[];
    this.arrSummary = new summary();
    this.Resturant = [];
    let total = 0;
    var  shortinfo = `${data.Adult}  Adult   ${data.Child}  Child `
    this.Resturant.push(
      {
        Name: data.arrRate.RestaurantName,
        Location: data.Details.Location + ' ' + data.Details.City,
        Meal: data.arrRate.Meal,
        PaxInfo: shortinfo,
        Data: JSON.stringify(data),
        Total: data.Total,
      }
    )
     service.push({ Hotel: [], Tour: [], Transfer:[] ,Restaurant: this.Resturant });
     this.arrSummary.location =[];
     this.arrSummary.location.push(this.summary.location[i]);
     this.arrSummary.location[0].Service = service;
     this.onSummarySave(this.arrSummary.location);
  }

  tourRemove(data: any, _location: any, i: any) {
    this.service.forEach(d => {
      var arrTour = d.Tour.filter(t => t.location == _location);
      if (arrTour.length != 0) {
        var sArr = d.Tour.find(t => t.location == _location && t.code == data);
        const index = d.Tour.indexOf(sArr);
        if (index > -1) {
          d.Tour.splice(index, 1);
        }
      }
    });
    this.summary.location[i].Service = this.service;
    console.log(JSON.stringify(this.summary));
  }

  _getSelectedTour(i: any) {
    let arrTourID = new Array();
    try {
      this.summary.location[i].Service.forEach(d => {
        d.Tour.forEach(t => {
          arrTourID.push(t.ID);
        })
      })
    } catch (e) { console.log(e) };
    return arrTourID
  }


  onSummarySave(location: any) {
    debugger
    console.log(location)
    this.common.SaveSummary(this.summary.FileNo, this.summary.QueryNo, location).subscribe((res: any) => {
      console.log(res)
      this.getSummary();
      this.cdr.detectChanges();
    }, err => {
      console.log(err);
    })
  }

  onskip(event: boolean) {
    this.skip = event
  }
   
  _getLocationTotal(index:any){
    debugger
    try{
      this.summary.location[index].price =0.00;
        this.summary.location[index].Service[0].Hotel.forEach(d=>{
          this.summary.location[index].price += parseFloat(d.total) 
         });
         this.summary.location[index].Service[0].Tour.forEach(d=>{
          this.summary.location[index].price += parseFloat(d.Total) 
         });
         this.summary.location[index].Service[0].Transfer.forEach(d=>{
          this.summary.location[index].price += parseFloat(d.Total) 
         });
         this.summary.location[index].Service[0].Restaurant.forEach(d=>{
          this.summary.location[index].price += parseFloat(d.Total) 
         });
         let result = 'Total';
         result = `Total : ${this.summary.location[index].price.toFixed(2)}`;
         return result;
    }catch(e){}
  }
}
