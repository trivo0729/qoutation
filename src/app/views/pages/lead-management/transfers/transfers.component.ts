import { Component, OnInit, ChangeDetectorRef, HostListener } from '@angular/core';
import { LeadManagementComponent } from '../lead-management.component';
import { arrLead } from '../../../../models/lead/quotation.model';
import { CommonService } from '../../../../services/common.service';
import { ToursService } from '../../../../services/sightseeing.service'; 
import { FormControl, FormGroup, FormBuilder, Validators, FormArray, ValidationErrors } from '@angular/forms';
import * as moment from 'moment';
import { NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'kt-transfers',
  templateUrl: './transfers.component.html',
  styleUrls: ['./transfers.component.scss']
})
export class TransfersComponent implements OnInit {

  constructor(
    private leadComponent: LeadManagementComponent,
    private common: CommonService,
    private tourService: ToursService,
    private cdr: ChangeDetectorRef,
    private fb: FormBuilder,
    config: NgbPaginationConfig
  ) { 
    config.size = 'sm';
    config.boundaryLinks = true;
  }
  pageSize = 5;
  page: number = 1;

  location: string;
  arrPickup:any[];
  arrDrop:any[];
  PickupID:number=0;
  PickupName:string;
  DropID:number=0;
  DropName:string;
  lead: arrLead;
  arrRates :any[];
  leadForm: FormGroup;
  loading:boolean = false;
  public PickupPoint: any[] = [
    { ID: "APT", Value: "Airport Transfers", selected: true, tooltip: "flight" },
    { ID: "ICT", Value: "Intercity Transfers", selected: false, tooltip: "location_city" },
    { ID: "IHT", Value: "Inter Hotel Transfers", selected: false, tooltip: "hotel" },
    { ID: "SST", Value: "Sightseeing Transfers", selected: false, tooltip: "filter_tilt_shift" },
    // { ID: "SIC", Value: "Sharing", selected: false, tooltip: "people_outline" }
  ]
 
  i: number = 0;
  ngOnInit() {
    this.right();
    this.initform();
    this.lead = this.common.getBasicDetails("lead");
    if (this.lead != undefined && this.lead.Destination.length != 0)
      this.location = this.lead.Destination[this.i].CityName;
    this._setDate();
  }
  public option: any = {
    locale: { format: 'DD-MM-YYYY' },
    alwaysShowCalendars: false,
    showDropdowns: true,
    autoApply: true,
    singleDatePicker: true,
    minDate: moment(new Date())
  };
  getComponentTitle() {
    let result = 'Transfers';
    if (!this.location) {
      return result;
    }
    result = `Transfers in ${this.location}`;
    return result;
  }

  _setTrip(code: string) {
    try {
      this.PickupPoint.forEach(d => {
        if (d.selected && d.ID != code)
          d.selected = false;
        else if (d.ID == code)
          d.selected = true;
      })
      this.arrDrop =[];
      this.arrPickup=[];
      this.PickupName ="";
      this.DropName="";
      this.arrRates =[];
    } catch (e) { }
  }
  _setDate() {
    this.option = {
      locale: { format: 'DD-MM-YYYY' },
      alwaysShowCalendars: false,
      showDropdowns: true,
      autoApply: true,
      singleDatePicker: true,
      startDate: moment(this.lead.Destination[this.i].Start, 'DD-MM-YYYY'),
      endDate: moment(this.lead.Destination[this.i].End, 'DD-MM-YYYY'),
      minDate: moment(this.lead.Destination[this.i].Start, 'DD-MM-YYYY'),
      maxDate: moment(this.lead.Destination[this.i].End, 'DD-MM-YYYY'),
    };
  }
  getLocation(event:any,Traveltype:string){
    let value =  "";
    if(event !="")
        value =  event.target.value;
    // if (value.length < 3 || (Traveltype == "D" && this.PickupID ==0))
    //   return
    debugger
    var PickupID=[];
    var  TripType = this.PickupPoint.find(d=>d.selected == true).ID
      if(Traveltype == "D")
       PickupID.push(this.PickupID)
    this.tourService.getTransferLocation(Traveltype,TripType,value,PickupID).subscribe((res: any) => {
      if (res.retCode === 1)
        {
          switch(Traveltype){
              case "P":
                this.arrPickup = res.arrLocation;
               
                break;
                case "D":
                this.arrDrop = res.arrLocation
                break;
          }
        }
      this.cdr.detectChanges();
    })
  }

  onselectlocation(ID:number,type:string){
    if(type == "P"){
      this.PickupID = ID;
      this.arrDrop =[];
      this.getLocation('',"D");
    }
    else
       this.DropID = ID;
  }

  _skipSightseeing() {
    try {

    } catch (e) { }
  }
  right() {
    this.leadComponent.onShowright(true);
  }

  search(){
    debugger
    this.loading  = true;
    try{
      this.lead = this.common.getBasicDetails("lead");
      var sCurrency =this.lead.Currency;
      var  Traveltype = this.PickupPoint.find(d=>d.selected == true);
      this.tourService.getTransferSearch(this.PickupID,this.DropID,'',Traveltype.ID,sCurrency).subscribe((res: any) => {
        if (res.retCode === 1)
          {
            this.loading  = false;
            this.arrRates = res.arrRates;
            this.arrRates.forEach(d=>{ d.PickupFrom = this.PickupName; d.DropOn= this.DropName; d.Traveltype  = Traveltype });
          }
        this.cdr.detectChanges();
      })
    }catch(e){}
  }
  _add(event:any,VehicleModule:any){
    debugger
    try{
        var arrData = this.arrRates.find(d=>d.VehicleModle == VehicleModule);
        if (event.checked)
        {
          this.leadComponent.makesummary(arrData, 'Transfer', this.lead.Destination[this.i].CityId, this.i);
        }
        else
          this.leadComponent.tourRemove(VehicleModule, this.lead.Destination[this.i].CityId, this.i);
    }catch(ex){}
  }
  initform() {
    this.leadForm = this.fb.group({
      Pickup: ['', Validators.compose([Validators.required])],
      Drop: ['', Validators.compose([Validators.required])],
    });
  }

  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.leadForm.controls[controlName];
    if (!control) {
      return false;
    }
    const result = control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }
}
