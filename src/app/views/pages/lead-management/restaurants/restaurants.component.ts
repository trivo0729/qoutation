import { Component, OnInit, ChangeDetectorRef, HostListener } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToursService } from '../../../../services/sightseeing.service';
import { LeadManagementComponent } from '../lead-management.component';
import { arrLead } from '../../../../models/lead/quotation.model';
import { CommonService } from '../../../../services/common.service';
import { RestaurantService } from '../../../../services/Restaurants.service';
import { NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap';
import { MatCheckboxChange } from '@angular/material';

@Component({
  selector: 'kt-restaurants',
  templateUrl: './restaurants.component.html',
  styleUrls: ['./restaurants.component.scss']
})
export class RestaurantsComponent implements OnInit {

  constructor( 
     private leadComponent: LeadManagementComponent, 
     private common: CommonService,
     private cdr: ChangeDetectorRef,
     private restService : RestaurantService,
     config: NgbPaginationConfig
     ) {  config.size = 'sm';
          config.boundaryLinks = true;
        }
  location: string = "";
  i: number = 0;
  lead: arrLead;
  bloder : boolean= false;
  arrList : any[];
  pageSize = 5;
  page: number = 1;
  ID:Number;
  show: boolean;
  mouse_is_inside: boolean = false;
  @HostListener('window:mouseup', ['$event'])
  onMouseUp() {
    if (!this.mouse_is_inside){
      this.arrList.forEach(d=>{
        d.show= false;
      });
    }
  }
  ngOnInit() {
    this.arrList =[];
    this.lead = this.common.getBasicDetails("lead");
    if (this.lead != undefined && this.lead.Destination.length != 0)
      this.location = this.lead.Destination[this.i].CityName;
    this.right();
    this.search()
  }

  onShow(ID:Number) {
    this.arrList.find(d=>d.ID == ID).show= true;
     this.cdr.detectChanges();
   }
   onMouseover(event: boolean,ID:number) {
     this.mouse_is_inside = event;
     this.ID = ID;
   }

  search() {
    debugger;
    this.bloder = true;
    var arrSelected = this.leadComponent._getSelectedTour(this.i);
    this.restService.getRestaurants(this.location,this.lead.Currency ).subscribe((res: any) => {
      if (res.retCode == 1) {
          this.arrList = res.arrRates;
          this.arrList.forEach((d,i)=>{
            d.ID = i;
            d.show = false;
            d.Total =  d.arrRate.Adult;
            d.Adult = 1;
            d.Child = 0;
          });
          this.bloder = false;
          this.cdr.detectChanges();
      }
    });
  }

  right() {
    this.leadComponent.onShowright(true);
  }
  getComponentTitle() {
    let result = 'Restaurants';
    if (!this.location) {
      return result;
    }
    result = `Restaurants in ${this.location}`;
    return result;
  }

  addPax(paxtype:string,ID:any){
    debugger
    switch(paxtype){
      case "ad":
        this.arrList.find(d=>d.ID == ID).Adult ++;
        break;
      case "ch":
        this.arrList.find(d=>d.ID == ID).Child ++;
        break;
    }  
    this._setTotal(ID)
  }
  removePax(paxtype:string,ID:any){
    switch(paxtype){
      case "ad":
        this.arrList.find(d=>d.ID == ID).Adult --;
        break;
      case "ch":
        this.arrList.find(d=>d.ID == ID).Child --;
        break;
    }  
    this._setTotal(ID)
  }

  _setTotal(ID:any){
    try{
      var Adult =   this.arrList.find(d=>d.ID == ID).Adult;
      var Child= this.arrList.find(d=>d.ID == ID).Child;
      var AdultRate =   this.arrList.find(d=>d.ID == ID).arrRate.Adult;  
      var ChildRate =   this.arrList.find(d=>d.ID == ID).arrRate.Child; 
      this.arrList.find(d=>d.ID == ID).Total = (parseFloat(Adult) *  parseFloat(AdultRate) + parseFloat(Child) *  parseFloat(ChildRate)).toFixed(2);
    }catch(e){console.log(e)}

  }

  _add(event: MatCheckboxChange,ID:Number) {
    debugger;
    try {
      var arrTour = this.arrList.find(d => d.ID == ID);
      if (event.checked)
      {
        this.leadComponent.makesummary(arrTour, 'Restaurant', this.lead.Destination[this.i].CityId, this.i);
      }
      else
        this.leadComponent.tourRemove(ID, this.lead.Destination[this.i].CityId, this.i);
    } catch (e) {
      console.log(e)
    }
  }
}
