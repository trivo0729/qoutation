// Angular
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
// Layout
import { LayoutConfigService } from '../../../core/_base/layout';
// Object-Path
import * as objectPath from 'object-path';
import { arrLead } from '../../../models/lead/quotation.model';
import { CommonService } from '../../../services/common.service';

@Component({
	selector: 'kt-subheader',
	templateUrl: './subheader.component.html',
})
export class SubheaderComponent implements OnInit {
	// Public properties
	// subheader layout
	layout: string;
	fluid: boolean;
    clear: boolean= true;
	lead: arrLead;
	destination: string;
	traveller: string;
	nationality: any;
    bShowLead : boolean = true
	/**
	 * Component constructor
	 *
	 * @param layoutConfigService: LayoutConfigService
	 */
	constructor(private layoutConfigService: LayoutConfigService,
		private common: CommonService,
		private cdr: ChangeDetectorRef
	) { }

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit(): void {
		const config = this.layoutConfigService.getConfig();

		this.layout = objectPath.get(config, 'subheader.layout');
		this.fluid = objectPath.get(config, 'footer.self.width') === 'fluid';
		this.clear = objectPath.get(config, 'subheader.clear');
		this.getBasicDetails('lead');
		if(window.location.pathname.includes('leads')|| window.location.pathname.includes('create-lead'))
			this.bShowLead = false;
	}
	onShow(show: boolean) {
		debugger
		this.bShowLead = show;
		this.cdr.detectChanges();
	  }

	getBasicDetails(name: string) {

		this.lead = this.common.getBasicDetails(name);
		if (this.lead)
			this.onResult();

	}

	onResult() {
		let destination = '';
		this.lead.Destination.forEach((d, index) => {
			if (index !== this.lead.Destination.length - 1)
				destination += `${d.CityName} (${d.Start} - ${d.End}) , `
			else
				destination += `${d.CityName} (${d.Start} - ${d.End})`
		});
		this.destination = destination;
		let i = this.lead.Queries.length - 1;
		let pax = this.lead.Queries[i].PaxDetails;
		this.traveller = `${pax.Adult} Adult , ${pax.Child} Child , ${pax.Infant} Infant`;
		let country = this.common.getCountry(this.lead.GuestNationality);
		this.nationality = country.name;
	}
}
